<div class="productLogs index">
	<div class="x_title" style="border: none">
		<h2><?php echo __('Report'); ?></h2>
		<div class="navbar-right">
			<label>
				<input type="checkbox" class="js-switch" checked /> Auto Refresh
			</label>
			<!-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-sm">Check</button> -->
		</div>
	</div>
	<div class="x_content">
		<table id="datatable" class="table table-striped table-bordered">
			<thead>
				<tr>
					<th class="text-center"><?php echo $this->Paginator->sort('Produk'); ?></th>
					<th class="text-center"><?php echo $this->Paginator->sort('Sub Produk'); ?></th>
					<th class="text-center"><?php echo $this->Paginator->sort('IDPEL'); ?></th>
					<th class="text-center"><?php echo $this->Paginator->sort('Status'); ?></th>
					<th class="text-center"><?php echo $this->Paginator->sort('Tanggal'); ?></th>
					<th class="text-center"><?php echo $this->Paginator->sort('Info'); ?></th>
				</tr>
			</thead>

			<tbody>	
				<?php foreach ($productLogs as $productLog): ?>
					<?php foreach($productLog as $log): ?>
						<tr>
							<td><?php echo h($log['categories']); ?></td>
							<td><?php echo h($log['product']); ?></td>
							<td><?php echo h($log['idpel']); ?></td>
							<td class="text-center">
								<?php if($log['status'] == "Normal") { ?>
									<button type="button" class="btn btn-success btn-xs"><?php echo h($log['status']); ?></button>
								<?php } else { ?>
									<button type="button" class="btn btn-danger btn-xs"><?php echo h($log['status']); ?></button> 
								<?php } ?>
							</td>
							<td class="text-center"><?php echo h($log['response_date']); ?></td>
							<td class="text-center">
								<?php if($log['status'] !== "Normal") { ?>
									<a class="btn btn-primary btn-xs" <?php echo $this->Html->link(__('Send Email'), array('action' => 'mail')); ?> </a>
								<?php } ?>
							</td>
						</tr>
					<?php endforeach; ?>
				<?php endforeach; ?>
			</tbody>
		</table>
	</div>
</div>

<!-- Modal -->
<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
				</button>
				<h4 class="modal-title" id="myModalLabel2">TXN Monitoring</h4>
			</div>
			<div class="modal-body">
			<?= $this->Form->create('ProductLogs',array('type' => 'post', 'class' => 'form-horizontal form-label-left', 'id' => 'hit-api-form', 'url' => array('controller' => 'ProductLogs/hit'))); ?>
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12">Product</label>
					<div class="col-md-9 col-sm-9 col-xs-12">
						<select class="form-control">
							<?php foreach($categories as $category): ?>
								<option value=""><?= $category['ProductCategory']['name']; ?></option>
							<?php endforeach; ?>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12">Sub Product</label>
					<div class="col-md-9 col-sm-9 col-xs-12">
						<select class="form-control">
							<?php foreach($products as $product): ?>
								<option value=""><?= $product['Product']['name']; ?></option>
							<?php endforeach; ?>
						</select>
					</div>
				</div>
				<!-- <div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">IDPEL <span class="required"></span>
					</label>
					<div class="col-md-9 col-sm-9 col-xs-12">
						<input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12">
					</div>
				</div> -->
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Check</button>
				</div>
			</form>
		</div>
	</div>
</div>
<!-- /modals -->
