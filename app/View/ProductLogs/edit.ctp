<div class="productLogs form">
<?php echo $this->Form->create('ProductLog'); ?>
	<fieldset>
		<legend><?php echo __('Edit Product Log'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('idpel');
		echo $this->Form->input('request');
		echo $this->Form->input('response');
		echo $this->Form->input('request_date');
		echo $this->Form->input('response_date');
		echo $this->Form->input('status');
		echo $this->Form->input('product_id');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('ProductLog.id')), array('confirm' => __('Are you sure you want to delete # %s?', $this->Form->value('ProductLog.id')))); ?></li>
		<li><?php echo $this->Html->link(__('List Product Logs'), array('action' => 'index')); ?></li>
		<!-- <li><?php echo $this->Html->link(__('List Products'), array('controller' => 'products', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Product'), array('controller' => 'products', 'action' => 'add')); ?> </li> -->
	</ul>
</div>
