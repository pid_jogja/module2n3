<div class="productLogs view">
<h2><?php echo __('Product Log'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($productLog['ProductLog']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Idpel'); ?></dt>
		<dd>
			<?php echo h($productLog['ProductLog']['idpel']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Request'); ?></dt>
		<dd>
			<?php echo h($productLog['ProductLog']['request']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Response'); ?></dt>
		<dd>
			<?php echo h($productLog['ProductLog']['response']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Request Date'); ?></dt>
		<dd>
			<?php echo h($productLog['ProductLog']['request_date']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Response Date'); ?></dt>
		<dd>
			<?php echo h($productLog['ProductLog']['response_date']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Status'); ?></dt>
		<dd>
			<?php echo h($productLog['ProductLog']['status']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Product'); ?></dt>
		<dd>
			<?php echo $this->Html->link($productLog['Product']['id'], array('controller' => 'products', 'action' => 'view', $productLog['Product']['id'])); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Product Log'), array('action' => 'edit', $productLog['ProductLog']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Product Log'), array('action' => 'delete', $productLog['ProductLog']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $productLog['ProductLog']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Product Logs'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Product Log'), array('action' => 'add')); ?> </li>
		<!-- <li><?php echo $this->Html->link(__('List Products'), array('controller' => 'products', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Product'), array('controller' => 'products', 'action' => 'add')); ?> </li> -->
	</ul>
</div>
