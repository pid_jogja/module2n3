<div class="productCategories form">
	<div class="x_title" style="border: none">
		<h2><?php echo __('Tambah Kategori Baru'); ?></h2>
	</div>
	<div class="x_content">
		<?php echo $this->Form->create('ProductCategory', array('url' => '/productCategories/add', 'class' => 'form-horizontal form-label-left', 'id' => 'form')) ?>
			
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-12">Nama</label>
				<div class="col-md-9 col-sm-9 col-xs-12">
					<input type="text" class="form-control" name="name">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-12">Deskripsi</label>
				<div class="col-md-9 col-sm-9 col-xs-12">
					<input type="text" class="form-control" name="description">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-12">Kode</label>
				<div class="col-md-9 col-sm-9 col-xs-12">
					<input type="text" class="form-control" name="code">
				</div>
			</div>
			
			<div class="ln_solid"></div>
			<div class="form-group">
				<div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
					<?php echo $this->Html->link(__('Kembali'), array('controller' => 'listpc'), array('class' => 'btn btn-primary')); ?>
					<button type="submit" class="btn btn-success" id="hit">Submit</button>
				</div>
			</div>
		</form>
	</div>
</div>
