<div class="productCategories view">
	<div class="x_title" style="border: none">
		<h2><?php echo __('Detail Kategori'); ?></h2>
		<div class="actions navbar-right">
			<?php echo $this->Html->link(__('Kembali'), array('controller' => 'listpc'), array('class' => 'btn btn-primary btn-sm')); ?>
		</div>
	</div>

	<div class="x_content">
		<div class="table-responsive">
			<table class="table table-bordered">
				<thead>
					<th class="text-center"><?php echo $this->Paginator->sort('ID'); ?></th>
					<th class="text-center"><?php echo $this->Paginator->sort('Nama'); ?></th>
					<th class="text-center"><?php echo $this->Paginator->sort('Deskripsi'); ?></th>
					<th class="text-center"><?php echo $this->Paginator->sort('Kode'); ?></th>
				</thead>
				<tbody>
					<tr>
						<td><?php echo h($productCategory['ProductCategory']['id']); ?></td>
						<td><?php echo h($productCategory['ProductCategory']['name']); ?></td>
						<td><?php echo h($productCategory['ProductCategory']['description']); ?></td>
						<td><?php echo h($productCategory['ProductCategory']['code']); ?></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>

