<div class="productCategories form">
	<div class="x_title" style="border: none">
		<h2><?php echo __('Edit Kategori'); ?></h2>
	</div>
	<div class="x_content">
			<?php echo $this->Form->create('ProductCategory', array('class' => 'form-horizontal form-label-left', 'id' => 'form')); ?>
 
			<div class="form-group">
				<div class="col-md-9 col-sm-9 col-xs-12">
					<?php echo $this->Form->input('id', array('label' => false , 'class' => 'form-control')); ?>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-12">Nama</label>
				<div class="col-md-9 col-sm-9 col-xs-12">
					<?php echo $this->Form->input('name', array('label' => false , 'class' => 'form-control')); ?>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-12">Deskripsi</label>
				<div class="col-md-9 col-sm-9 col-xs-12">
					<?php echo $this->Form->input('description', array('label' => false , 'class' => 'form-control')); ?>
				</div>
			</div>
			
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-12">Kode</label>
				<div class="col-md-9 col-sm-9 col-xs-12">
					<?php echo $this->Form->input('code', array('label' => false , 'class' => 'form-control')); ?>
				</div>
			</div>
			

			<div class="ln_solid"></div>
			<div class="form-group">
				<div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
					<?php echo $this->Html->link(__('Kembali'), array('action' => 'index'), array('class' => 'btn btn-primary')); ?>
					<button type="submit" class="btn btn-success" id="hit">Submit</button>
				</div>
			</div>
		</form>
	</div>
</div>

