<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

// $cakeDescription = __d('cake_dev', 'CakePHP: the rapid development php framework');
// $cakeVersion = __d('cake_dev', 'CakePHP %s', Configure::version())
?>


<!DOCTYPE html>
<html lang="en">
  <head>
    <?php echo $this->Html->charset(); ?>
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Txn Monitoring</title>

    <?php
        // <!-- Favicon-->
        echo $this->Html->meta('icon');
        echo $this->fetch('meta');
        echo $this->fetch('css');
        echo $this->fetch('script');
    ?>

    <!-- Bootstrap -->
    <?php echo $this->Html->css('vendors/bootstrap/dist/css/bootstrap.min.css') ?>
    <!-- Font Awesome -->
    <?php echo $this->Html->css('vendors/font-awesome/css/font-awesome.min.css') ?>
    <!-- NProgress -->
    <?php echo $this->Html->css('vendors/nprogress/nprogress.css') ?>
    <!-- iCheck -->
    <?php echo $this->Html->css('vendors/iCheck/skins/flat/green.css') ?>
	
    <!-- bootstrap-progressbar -->
    <?php echo $this->Html->css('vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css') ?>
    <!-- JQVMap -->
    <?php echo $this->Html->css('vendors/jqvmap/dist/jqvmap.min.css') ?>
    <!-- bootstrap-daterangepicker -->
    <?php echo $this->Html->css('vendors/bootstrap-daterangepicker/daterangepicker.css') ?>

    <!-- Datatables -->
    <?php echo $this->Html->css('vendors/datatables.net-bs/css/dataTables.bootstrap.min.css') ?>
    <?php echo $this->Html->css('vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css') ?>
    <?php echo $this->Html->css('vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css') ?>
    <?php echo $this->Html->css('vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css') ?>
    <?php echo $this->Html->css('vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css') ?>

    <!-- Switchery -->
    <?php echo $this->Html->css('vendors/switchery/dist/switchery.min.css') ?>

    <!-- Custom Theme Style -->
    <?php echo $this->Html->css('build/css/custom.min.css') ?>
    <!-- SweetAlert -->
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
    <!-- JQuery -->
    <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
  crossorigin="anonymous"></script>
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="<?= $this->webroot; ?>" class="site_title"><i class="fa fa-home"></i> <span>Txn Monitoring</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3><br />MENU UTAMA</h3>
                <ul class="nav side-menu">
                  <li>
                    <a href="<?= $this->webroot; ?>check-status"><i class="fa fa-refresh"></i> Cek Status</a>
                  </li>
                  <!-- <li>
                    <a href="<?= $this->webroot; ?>report"><i class="fa fa-table"></i> Laporan</a>
                  </li> -->
                  <li class>
                    <a><i class="fa fa-table"></i> Laporan <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu" style="display: none;">
                        <li>
                          <?= $this->Html->link("Status Inquiry", "/inquiry"); ?>
                        </li>
                        <li>
                          <?= $this->Html->link("Status Payment", "/payment"); ?>
                        </li>
                    </ul>
                  </li>
                  <li class>
                    <a><i class="fa fa-cog"></i> Pengaturan <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu" style="display: none;">
                        <li>
                          <?= $this->Html->link("ID Pelanggan", array('controller' => 'pelanggan', 'action' => 'index')); ?>
                        </li>
                        <li>
                          <?= $this->Html->link("Produk", array('controller' => 'Products', 'action' => 'look')); ?>
                        </li>
                        <li>
                          <?= $this->Html->link("Kategori", array('controller' => 'listpc', 'action' => 'index')); ?>
                        </li>
                    </ul>
                  </li>
                </ul>
              </div>
            </div>
            <!-- /sidebar menu -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    Sistem Monitoring
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    <li><a href="javascript:;">Refresh</a></li>
                    <li><a href="javascript:;">Bantuan</a></li>
                  </ul>
                </li>
              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <?php echo $this->Flash->render(); ?>
                    <?php echo $this->fetch('content'); ?>

                  <div class="clearfix"></div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
            Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com">Colorlib</a>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <?php echo $this->Html->script('vendors/jquery/dist/jquery.min.js'); ?>
    <!-- Bootstrap -->
    <?php echo $this->Html->script('vendors/bootstrap/dist/js/bootstrap.min.js'); ?>
    <!-- FastClick -->
    <?php echo $this->Html->script('vendors/fastclick/lib/fastclick.js'); ?>
    <!-- NProgress -->
    <?php echo $this->Html->script('vendors/nprogress/nprogress.js'); ?>
    <!-- Chart.js -->
    <?php echo $this->Html->script('vendors/Chart.js/dist/Chart.min.js'); ?>
    <!-- gauge.js -->
    <?php echo $this->Html->script('vendors/gauge.js/dist/gauge.min.js'); ?>
    <!-- bootstrap-progressbar -->
    <?php echo $this->Html->script('vendors/bootstrap-progressbar/bootstrap-progressbar.min.js'); ?>
    <!-- iCheck -->
    <?php echo $this->Html->script('vendors/iCheck/icheck.min.js'); ?>
    <!-- Datatables -->
    <?php echo $this->Html->script('vendors/datatables.net/js/jquery.dataTables.min.js'); ?>
    <?php echo $this->Html->script('vendors/datatables.net-bs/js/dataTables.bootstrap.min.js'); ?>
    <?php echo $this->Html->script('vendors/datatables.net-buttons/js/dataTables.buttons.min.js'); ?>
    <?php echo $this->Html->script('vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js'); ?>
    <?php echo $this->Html->script('vendors/datatables.net-buttons/js/buttons.flash.min.js'); ?>
    <?php echo $this->Html->script('vendors/datatables.net-buttons/js/buttons.html5.min.js'); ?>
    <?php echo $this->Html->script('vendors/datatables.net-buttons/js/buttons.print.min.js'); ?>
    <?php echo $this->Html->script('vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js'); ?>
    <?php echo $this->Html->script('vendors/datatables.net-keytable/js/dataTables.keyTable.min.js'); ?>
    <?php echo $this->Html->script('vendors/datatables.net-responsive/js/dataTables.responsive.min.js'); ?>
    <?php echo $this->Html->script('vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js'); ?>
    <?php echo $this->Html->script('vendors/datatables.net-scroller/js/dataTables.scroller.min.js'); ?>
    <!-- Skycons -->
    <?php echo $this->Html->script('vendors/skycons/skycons.js'); ?>
    <!-- Flot -->
    <?php echo $this->Html->script('vendors/Flot/jquery.flot.js'); ?>
    <?php echo $this->Html->script('vendors/Flot/jquery.flot.pie.js'); ?>
    <?php echo $this->Html->script('vendors/Flot/jquery.flot.time.js'); ?>
    <?php echo $this->Html->script('vendors/Flot/jquery.flot.stack.js'); ?>
    <?php echo $this->Html->script('vendors/Flot/jquery.flot.resize.js'); ?>
    <!-- Flot plugins -->
    <?php echo $this->Html->script('vendors/flot.orderbars/js/jquery.flot.orderBars.js'); ?>
    <?php echo $this->Html->script('vendors/flot-spline/js/jquery.flot.spline.min.js'); ?>
    <?php echo $this->Html->script('vendors/flot.curvedlines/curvedLines.js'); ?>
    <!-- DateJS -->
    <?php echo $this->Html->script('vendors/DateJS/build/date.js'); ?>
    <!-- JQVMap -->
    <?php echo $this->Html->script('vendors/jqvmap/dist/jquery.vmap.js'); ?>
    <?php echo $this->Html->script('vendors/jqvmap/dist/maps/jquery.vmap.world.js'); ?>
    <?php echo $this->Html->script('vendors/jqvmap/examples/js/jquery.vmap.sampledata.js'); ?>
    <!-- bootstrap-daterangepicker -->
    <?php echo $this->Html->script('vendors/moment/min/moment.min.js'); ?>
    <?php echo $this->Html->script('vendors/bootstrap-daterangepicker/daterangepicker.js'); ?>
    <!-- Switchery -->
    <?php echo $this->Html->script('vendors/switchery/dist/switchery.min.js'); ?>

    <!-- Custom Theme Scripts -->
    <?php echo $this->Html->script('build/js/custom.min.js'); ?>
	
  </body>
</html>




