<div class="products view">
	<div class="x_title" style="border: none">
		<h2><?php echo __('Detail Produk'); ?></h2>
		<div class="actions navbar-right">
			<?php echo $this->Html->link(__('Kembali'), array('controller' => 'lookp'), array('class' => 'btn btn-primary btn-sm')); ?>
		</div>
	</div>

	<div class="x_content">
		<div class="table-responsive">
			<table class="table table-bordered">
				<thead>
					<th class="text-center"><?php echo $this->Paginator->sort('ID'); ?></th>
					<th class="text-center"><?php echo $this->Paginator->sort('Nama'); ?></th>
					<th class="text-center"><?php echo $this->Paginator->sort('Pan'); ?></th>
					<th class="text-center"><?php echo $this->Paginator->sort('Deskripsi'); ?></th>
					<th class="text-center"><?php echo $this->Paginator->sort('Kode'); ?></th>
					<th class="text-center"><?php echo $this->Paginator->sort('Kategori'); ?></th>
				</thead>
				<tbody>
					<tr>
						<td><?php echo h($product['Product']['id']); ?></td>
						<td><?php echo h($product['Product']['name']); ?></td>
						<td><?php echo h($product['Product']['pan']); ?></td>
						<td><?php echo h($product['Product']['description']); ?></td>
						<td><?php echo h($product['Product']['code']); ?></td>
						<td><?php echo h($product['ProductCategory']['name']) ?></td>
					</tr>
				</tbody>
			</table>
		</div>

<!-- 		
		<h4><?php echo __('Related Product Logs'); ?></h4>
		<?php if (!empty($product['ProductLog'])): ?>
		<div class="table-responsive">
			<table class="table table-bordered">
				<thead>
					<th><?php echo __('ID'); ?></th>
					<th><?php echo __('Idpel'); ?></th>
					<th><?php echo __('Request'); ?></th>
					<th style="width:50px;"><?php echo __('Response'); ?></th>
					<th><?php echo __('Request Date'); ?></th>
					<th><?php echo __('Response Date'); ?></th>
					<th><?php echo __('Status'); ?></th>
					<th><?php echo __('Product Id'); ?></th>
					<th><?php echo __('Actions'); ?></th>
				</thead>
				<tbody>
				<?php foreach ($product['ProductLog'] as $productLog): ?>
					<tr>
						<td><?php echo $productLog['id']; ?></td>
						<td><?php echo $productLog['idpel']; ?></td>
						<td><?php echo $productLog['request']; ?></td>
						<td><?php echo $productLog['response']; ?></td>
						<td><?php echo $productLog['request_date']; ?></td>
						<td><?php echo $productLog['response_date']; ?></td>
						<td><?php echo $productLog['status']; ?></td>
						<td><?php echo $productLog['product_id']; ?></td>
						<td class="actions">
							<?php echo $this->Html->link(__('View'), array('controller' => 'product_logs', 'action' => 'view', $productLog['id'])); ?>
							<?php echo $this->Html->link(__('Edit'), array('controller' => 'product_logs', 'action' => 'edit', $productLog['id'])); ?>
							<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'product_logs', 'action' => 'delete', $productLog['id']), array('confirm' => __('Are you sure you want to delete # %s?', $productLog['id']))); ?>
						</td>
					</tr>
				<?php endforeach; ?>
				</tbody>
			</table>
			<?php endif; ?>	 -->
		</div>
	</div>
</div>







<!-- 
<div class="products view">

<div class="related">
	<h3><?php echo __('Related Product Logs'); ?></h3>
	<?php if (!empty($product['ProductLog'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Idpel'); ?></th>
		<th><?php echo __('Request'); ?></th>
		<th><?php echo __('Response'); ?></th>
		<th><?php echo __('Request Date'); ?></th>
		<th><?php echo __('Response Date'); ?></th>
		<th><?php echo __('Status'); ?></th>
		<th><?php echo __('Product Id'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($product['ProductLog'] as $productLog): ?>
		<tr>
			<td><?php echo $productLog['id']; ?></td>
			<td><?php echo $productLog['idpel']; ?></td>
			<td><?php echo $productLog['request']; ?></td>
			<td><?php echo $productLog['response']; ?></td>
			<td><?php echo $productLog['request_date']; ?></td>
			<td><?php echo $productLog['response_date']; ?></td>
			<td><?php echo $productLog['status']; ?></td>
			<td><?php echo $productLog['product_id']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'product_logs', 'action' => 'view', $productLog['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'product_logs', 'action' => 'edit', $productLog['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'product_logs', 'action' => 'delete', $productLog['id']), array('confirm' => __('Are you sure you want to delete # %s?', $productLog['id']))); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Product Log'), array('controller' => 'product_logs', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Product Samples'); ?></h3>
	<?php if (!empty($product['ProductSample'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Idpel'); ?></th>
		<th><?php echo __('Nominal'); ?></th>
		<th><?php echo __('Status'); ?></th>
		<th><?php echo __('Product Id'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($product['ProductSample'] as $productSample): ?>
		<tr>
			<td><?php echo $productSample['id']; ?></td>
			<td><?php echo $productSample['idpel']; ?></td>
			<td><?php echo $productSample['nominal']; ?></td>
			<td><?php echo $productSample['status']; ?></td>
			<td><?php echo $productSample['product_id']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'product_samples', 'action' => 'view', $productSample['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'product_samples', 'action' => 'edit', $productSample['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'product_samples', 'action' => 'delete', $productSample['id']), array('confirm' => __('Are you sure you want to delete # %s?', $productSample['id']))); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Product Sample'), array('controller' => 'product_samples', 'action' => 'add')); ?> </li>
		</ul>
	</div> -->
<!-- </div> -->
