
<div class="actions navbar-right">
	<?php echo $this->Html->link(__('Tambah Produk Baru'), array('action' => 'add'), array('class' => 'btn btn-primary btn-sm')); ?>
</div>


<div class="product index">
	<div class="x_title" style="border: none">
		<h2><?php echo __('Daftar Produk'); ?></h2>
	</div>
	<div class="x_content">
		<div class="table-responsive">
			<table class="table table-striped table-bordered" id="datatable">
				<thead>
					<th class="text-center"><?php echo __('#'); ?></th>
					<th class="text-center"><?php echo __('Nama'); ?></th>
					<th class="text-center"><?php echo __('Deskripsi'); ?></th>
					<th class="text-center"><?php echo __('Kode'); ?></th>
					<th class="actions text-center" style="width:200px"><?php echo __('Opsi'); ?></th>
				</thead>
				<tbody>
					<?php $no = 1; foreach ($products as $product): ?>
						<tr>
							<td><?php echo $no; ?></td>
							<td><?php echo ($product['Product']['name']); ?></td>
							<td><?php echo ($product['Product']['description']); ?></td>
							<td><?php echo ($product['Product']['code']); ?></td>
							<td class="actions text-center">
								<?php echo $this->Html->link(__('Detail'), array('action' => 'view', $product['Product']['id']), array('class' => 'btn btn-info btn-xs')); ?>
								<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $product['Product']['id']), array('class' => 'btn btn-warning btn-xs')); ?>
								<?php echo $this->Form->postLink(__('Hapus'), array('action' => 'delete', $product['Product']['id']), array('class' => 'btn btn-danger btn-xs', 'confirm' => __('Hapus data ini?'))); ?>
							</td>
						</tr>
					<?php $no++; endforeach; ?>
				</tbody>
			</table>
		</div>
	</div>
</div>
