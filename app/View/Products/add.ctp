
<div class="product form">
	<div class="x_title" style="border: none">
		<h2><?php echo __('Tambah Produk Baru'); ?></h2>
	</div>
	<div class="x_content">
		<?php echo $this->Form->create('Product', array('url' => '/Products/add', 'class' => 'form-horizontal form-label-left', 'id' => 'form')) ?>
			
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-12">Nama</label>
				<div class="col-md-9 col-sm-9 col-xs-12">
					<input type="text" class="form-control" name="name">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-12">Deskripsi</label>
				<div class="col-md-9 col-sm-9 col-xs-12">
					<input type="text" class="form-control" name="description">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-12">Kode</label>
				<div class="col-md-9 col-sm-9 col-xs-12">
					<input type="text" class="form-control" name="code">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-12">Pan</label>
				<div class="col-md-9 col-sm-9 col-xs-12">
					<input type="text" class="form-control" name="pan">
				</div>
			</div>
			
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-12">Kategori</label>
				<div class="col-md-9 col-sm-9 col-xs-12">
						<select class="form-control" id="categories" name="product_category_id">
							<option value="">-- Pilih Kategori--</option>
							<?php foreach($categories as $category): ?>
								<option value="<?= $category['ProductCategory']['id'];?>"><?= $category['ProductCategory']['name']; ?></option>
							<?php endforeach; ?>
						</select>
				</div>
			</div>

			<div class="ln_solid"></div>
			<div class="form-group">
				<div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
					<?php echo $this->Html->link(__('Kembali'), array('controller' => 'lookp'), array('class' => 'btn btn-primary')); ?>
					<button type="submit" class="btn btn-success" id="hit">Submit</button>
				</div>
			</div>
		</form>
	</div>
</div>

