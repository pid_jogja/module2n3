<div class="products index">
	<div class="x_title" style="border: none">
		<h2><?php echo __('Cek Status'); ?></h2>
	</div>
	<div class="x_content">
		<?php echo $this->Form->create('Product', array('url' => '/ProductLogs/hit', 'class' => 'form-horizontal form-label-left', 'id' => 'form')) ?>
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-12">Kategori</label>
				<div class="col-md-9 col-sm-9 col-xs-12">
						<select class="form-control" id="categories" name="categories">
							<option value="">-- Pilih Kategori --</option>
							<?php foreach($categories as $category): ?>
								<option value="<?= $category['ProductCategory']['id']; ?>-<?= $category['ProductCategory']['name']; ?>"><?= $category['ProductCategory']['name']; ?></option>
							<?php endforeach; ?>
						</select>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-12">Produk</label>
				<div class="col-md-9 col-sm-9 col-xs-12">
						<select class="form-control" id="product" name="product" disabled>
						</select>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-12">ID Pelanggan</label>
				<div class="col-md-9 col-sm-9 col-xs-12">
					<input type="text" class="form-control" name="idpel">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-12">Periode</label>
				<div class="col-md-9 col-sm-9 col-xs-12">
					<input type="text" class="form-control" disabled id="periode" name="periode">
				</div>
			</div>

			<div class="ln_solid"></div>
			<div class="form-group">
				<div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
					<button type="submit" class="btn btn-success" id="hit">Submit</button>
				</div>
			</div>
		</form>
	</div>
</div>


<!-- <table id="datatable" class="table table-striped table-bordered">
	<thead>
		<tr>
			<th class="text-center"><?php echo $this->Paginator->sort('Produk'); ?></th>
			<th class="text-center"><?php echo $this->Paginator->sort('Sub Produk'); ?></th>
			<th class="text-center"><?php echo $this->Paginator->sort('IDPEL'); ?></th>
			<th class="text-center"><?php echo $this->Paginator->sort('Status'); ?></th>
		</tr>
	</thead>

	<tbody id="table">	

	</tbody>
</table> -->

<table class="table table-bordered">
	<thead>
	<tr>
		<th class="text-center"><?php echo $this->Paginator->sort('Kategori'); ?></th>
		<th class="text-center"><?php echo $this->Paginator->sort('Produk'); ?></th>
		<th class="text-center"><?php echo $this->Paginator->sort('ID Pelangan'); ?></th>
		<th class="text-center"><?php echo $this->Paginator->sort('Status'); ?></th>
	</tr>
	</thead>
	<tbody>

	</tbody>
</table>

<script>

	$(document).ready(function(){
		$("#categories").on('change',function() {
			
			var id = $(this).val();
			
			if (id == "") {
				$("#product").prop("disabled", true);
			}

			$("#product").find('option').remove();
			if (id) {
				// var dataString = 'id='+ id;
				$.ajax({
					dataType:'json',
					type: "POST",
					evalScripts: true,
					url: '<?php echo Router::url(array("controller" => "Products", "action" => "getProducts")); ?>' ,
					data: {id : id},
					cache: false,
					success: function(response) {
						$("#product").prop("disabled", false);
						$.each(response, function(key, value) {              
							$.each(value, function(k, v) {              
								$('<option>').val(v.code+'-'+v.name).text(v.name).appendTo($("#product"));
							});
						});
					} 
				});
			}

			$("#periode").prop('disabled', true);

			if ($("#categories").val() == "2-BPJS") {
				$("#periode").prop('disabled', false);
			}
		});


		$("#hit").on('click', function(e) {
			e.preventDefault();

			$(".table > tbody>tr:first-child").remove();

			var btnPeriode = $("#periode");

			if($("#categories").val() == "") {
				swal('warning', 'Oops...' , 'Please Choose Product!');
			} else if(btnPeriode.prop("disabled") == false && btnPeriode.val() == "") {
				swal('warning', 'Oops...' , 'Please Enter Periode!');
			} else {
				var formData = $("#form").serialize();
			
				var xhr = $.ajax({
					dataType:'json',
					type: "POST",
					evalScripts: true,
					url: '<?php echo Router::url(array("controller" => "ProductLogs", "action" => "hit")); ?>' ,
					data: formData,
					cache: false,
					beforeSend: function(){
						swal('warning', '', 'Waiting ...');
					},
					success: function(data) {
						if (data.status == "Normal") {
							Swal.fire({
								type: 'success',
								text: 'RC ['+data.rc+'] - Request Success'
							}).then(function() {
								let html = '<tr>' +  
												'<td>'+data.category+'</td>' +
												'<td>'+data.product+'</td>' +
												'<td class="text-center">'+data.idpel+'</td>' +
												'<td class="text-center"><button class="btn btn-success btn-xs">'+data.status+'</button></td>' +
											'</tr>';
								$(".table").append(html);
							})
						} else if (data.emptyid == false) {
							swal('warning', 'Oops...', 'Please Enter Your IDPEL!');
						} else {
							Swal.fire({
								type: 'error',
								title: 'Oops...',
								text: 'RC ['+data.rc+'] - Request Failed'
							}).then(function() {
								let html = '<tr>' +  
												'<td>'+data.category+'</td>' +
												'<td>'+data.product+'</td>' +
												'<td class="text-center">'+data.idpel+'</td>' +
												'<td class="text-center"><button class="btn btn-danger btn-xs">'+data.status+'</button></td>' +
											'</tr>';
								$(".table").append(html);
							})
						}
					}
				});
			}
		});
	});

	function swal(type, title , message) {
		Swal.fire({
			type: type,
			title: title,
			text: message
		})
	}

</script>