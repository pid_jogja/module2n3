
<div class="actions navbar-right">
	<?php echo $this->Html->link(__('Tambah Idpel Baru'), array('action' => 'add'), array('class' => 'btn btn-primary btn-sm')); ?>
</div>
<div class="productSamples index">
	<div class="x_title" style="border: none">
		<h2><?php echo __('Daftar ID Pelanggan'); ?></h2>
	</div>
	<div class="x_content">
		<div class="table-responsive">
			<table class="table table-striped table-bordered" id="datatable">
				<thead>
					<th class="text-center"><?php echo __('#'); ?></th>
					<th class="text-center"><?php echo __('ID Pelanggan'); ?></th>
					<th class="text-center"><?php echo __('Produk'); ?></th>
					<th class="actions text-center" style="width:200px"><?php echo __('Opsi'); ?></th>
				</thead>

				<tbody>
					<?php $no = 1; foreach ($productSamples as $productSample): ?>
					<tr>
						<td><?php echo $no; ?></td>
						<td><?php echo ($productSample['ProductSample']['idpel']); ?></td>
						<td>
							<?php echo $this->Html->link($productSample['Product']['name'], array('controller' => 'products', 'action' => 'view', $productSample['Product']['id'])); ?>
						</td>
						<td class="actions text-center">
							<?php echo $this->Html->link(__('Detail'), array('action' => 'view', $productSample['ProductSample']['id']), array('class' => 'btn btn-info btn-xs')); ?>
							<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $productSample['ProductSample']['id']), array('class' => 'btn btn-warning btn-xs')); ?>
							<?php echo $this->Form->postLink(__('Hapus'), array('action' => 'delete', $productSample['ProductSample']['id']), array('class' => 'btn btn-danger btn-xs', 'confirm' => __('Hapus data ini?'))); ?>
						</td>
					</tr>
					<?php $no++; endforeach; ?>
				</tbody>
			</table>
		</div>
	</div>
</div>
