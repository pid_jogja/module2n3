<div class="productSamples form">
	<div class="x_title" style="border: none">
		<h2><?php echo __('Tambah ID Pelanggan Baru'); ?></h2>
	</div>
	<div class="x_content">
		<?php echo $this->Form->create('ProductSample', array('url' => '/ProductSamples/add', 'class' => 'form-horizontal form-label-left', 'id' => 'form')) ?>
			

			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-12">ID Pelanggan</label>
				<div class="col-md-9 col-sm-9 col-xs-12">
					<input type="text" class="form-control" name="idpel">
				</div>
			</div>
			<div class="form-group" style="display:none;">
				<label class="control-label col-md-3 col-sm-3 col-xs-12">Nominal</label>
				<div class="col-md-9 col-sm-9 col-xs-12">
					<input type="text" class="form-control" name="nominal" value="0">
				</div>
			</div>
			<div class="form-group" style="display:none;">
				<label class="control-label col-md-3 col-sm-3 col-xs-12">Status</label>
				<div class="col-md-9 col-sm-9 col-xs-12">
					<input type="text" name="status" align="left" value="true">
				</div>
			</div>
			
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-12">Produk</label>
				<div class="col-md-9 col-sm-9 col-xs-12">
						<select class="form-control" id="product" name="product_id" required>
							<option value="">-- Pilih Produk--</option>
							<?php foreach($products as $product): ?>
								<option value="<?= $product['Product']['id']; ?>"><?= $product['Product']['name']; ?></option>
							<?php endforeach; ?>
						</select>
				</div>
			</div>

			<div class="ln_solid"></div>
			<div class="form-group">
				<div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
					<?php echo $this->Html->link(__('Kembali'), array('action' => 'index'), array('class' => 'btn btn-primary')); ?>

					<button type="submit" class="btn btn-success">Submit</button>
				</div>
			</div>
		</form>
	</div>
</div>













