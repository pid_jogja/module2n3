<div class="productSamples index">
	<div class="x_title" style="border: none">
		<h2><?php echo __('Detail ID Pelanggan'); ?></h2>
		<div class="actions navbar-right">
			<?php echo $this->Html->link(__('Kembali'), array('action' => 'index'), array('class' => 'btn btn-primary btn-sm')); ?>
		</div>
	</div>

	<div class="x_content">
		<div class="table-responsive">
			<table class="table table-bordered">
				<thead>
					<th class="text-center"><?php echo $this->Paginator->sort('ID'); ?></th>
					<th class="text-center"><?php echo $this->Paginator->sort('ID Pelanggan'); ?></th>
					<th><?php echo $this->Paginator->sort('Produk'); ?></th>
					<th><?php echo $this->Paginator->sort('Kategori'); ?></th>
					<th class="text-center"><?php echo $this->Paginator->sort('Produk ID'); ?></th>
				</thead>
				<tbody>
					<tr>
						<td><?php echo h($productSample['ProductSample']['id']); ?></td>
						<td><?php echo h($productSample['ProductSample']['idpel']); ?></td>
						<td><?php echo h($productSample['Product']['name']); ?></td>
						<td><?php echo h($productSample['Product']['ProductCategory']['name']); ?></td>
						<td><?php echo h($productSample['Product']['id']) ?></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>
