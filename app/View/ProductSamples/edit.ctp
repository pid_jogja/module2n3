<div class="productSamples form">
	<div class="x_title" style="border: none">
		<h2><?php echo __('Edit ID Pelanggan'); ?></h2>
	</div>
	<div class="x_content">
			<?php echo $this->Form->create('ProductSample', array('class' => 'form-horizontal form-label-left', 'id' => 'form')); ?>

			<div class="form-group">
				<div class="col-md-9 col-sm-9 col-xs-12">
					<?php echo $this->Form->input('id'); ?>
				</div>
			</div>

			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-12">ID Pelanggan</label>
				<div class="col-md-9 col-sm-9 col-xs-12">
					<?php echo $this->Form->input('idpel', array('label' => false , 'class' => 'form-control')); ?>
				</div>
			</div>
			<div class="form-group" style="display:none;">
				<label class="control-label col-md-3 col-sm-3 col-xs-12">Nominal</label>
				<div class="col-md-9 col-sm-9 col-xs-12">
					<?php echo $this->Form->input('nominal', array('label' => false , 'class' => 'form-control')); ?>
				</div>
			</div>
			<div class="form-group" style="display:none;">
				<label class="control-label col-md-3 col-sm-3 col-xs-12">Status</label>
				<div class="col-md-9 col-sm-9 col-xs-12">
					<?php echo $this->Form->input('status', array('label' => false , 'class' => 'form-control')); ?>
				</div>
			</div>
			
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-12">Produk</label>
				<div class="col-md-9 col-sm-9 col-xs-12">
						<?php echo $this->Form->input('product_id', array(
							'type' => 'select',
							'label'	=> false,
							'class' => 'form-control'
						)); ?>
				</div>
			</div>

			<div class="ln_solid"></div>
			<div class="form-group">
				<div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
					<?php echo $this->Html->link(__('Kembali'), array('action' => 'index'), array('class' => 'btn btn-primary')); ?>
					<button type="submit" class="btn btn-success" id="hit">Submit</button>
				</div>
			</div>
		</form>
	</div>
</div>
