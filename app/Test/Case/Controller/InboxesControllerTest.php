<?php
App::uses('InboxesController', 'Controller');

/**
 * InboxesController Test Case
 */
class InboxesControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.inbox'
	);

}
