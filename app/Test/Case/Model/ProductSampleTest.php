<?php
App::uses('ProductSample', 'Model');

/**
 * ProductSample Test Case
 */
class ProductSampleTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.product_sample',
		'app.product'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->ProductSample = ClassRegistry::init('ProductSample');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->ProductSample);

		parent::tearDown();
	}

}
