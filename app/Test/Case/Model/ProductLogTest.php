<?php
App::uses('ProductLog', 'Model');

/**
 * ProductLog Test Case
 */
class ProductLogTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.product_log',
		'app.product'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->ProductLog = ClassRegistry::init('ProductLog');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->ProductLog);

		parent::tearDown();
	}

}
