<?php
/**
 * ProductSample Fixture
 */
class ProductSampleFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'biginteger', 'null' => false, 'default' => null, 'length' => 11, 'key' => 'primary'),
		'idpel' => array('type' => 'text', 'null' => true, 'default' => null),
		'nominal' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 45),
		'status' => array('type' => 'boolean', 'null' => true, 'default' => null),
		'product_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('unique' => true, 'column' => 'id')
		),
		'tableParameters' => array()
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'idpel' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
			'nominal' => 'Lorem ipsum dolor sit amet',
			'status' => 1,
			'product_id' => 1
		),
	);

}
