<?php
/**
 * ProductCategory Fixture
 */
class ProductCategoryFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'biginteger', 'null' => false, 'default' => null, 'length' => 11, 'key' => 'primary'),
		'name' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 45),
		'description' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 45),
		'code' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 45),
		'indexes' => array(
			'PRIMARY' => array('unique' => true, 'column' => 'id')
		),
		'tableParameters' => array()
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'name' => 'Lorem ipsum dolor sit amet',
			'description' => 'Lorem ipsum dolor sit amet',
			'code' => 'Lorem ipsum dolor sit amet'
		),
	);

}
