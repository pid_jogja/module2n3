<?php
/**
 * ProductLog Fixture
 */
class ProductLogFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'biginteger', 'null' => false, 'default' => null, 'length' => 11, 'key' => 'primary'),
		'idpel' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 45),
		'request' => array('type' => 'text', 'null' => true, 'default' => null, 'length' => 1073741824),
		'response' => array('type' => 'text', 'null' => true, 'default' => null, 'length' => 1073741824),
		'request_date' => array('type' => 'time', 'null' => true, 'default' => null),
		'response_date' => array('type' => 'time', 'null' => true, 'default' => null),
		'status' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 45),
		'product_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('unique' => true, 'column' => 'id')
		),
		'tableParameters' => array()
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'idpel' => 'Lorem ipsum dolor sit amet',
			'request' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
			'response' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
			'request_date' => '04:33:12',
			'response_date' => '04:33:12',
			'status' => 'Lorem ipsum dolor sit amet',
			'product_id' => 1
		),
	);

}
