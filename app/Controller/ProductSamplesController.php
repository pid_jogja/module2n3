<?php
App::uses('AppController', 'Controller');
/**
 * ProductSamples Controller
 *
 * @property ProductSample $ProductSample
 * @property PaginatorComponent $Paginator
 */
class ProductSamplesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->ProductSample->recursive = 0;

		$ps = $this->ProductSample->find('all');
		$this->set('productSamples', $ps);
	}	

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->ProductSample->recursive = 2;
		if (!$this->ProductSample->exists($id)) {
			throw new NotFoundException(__('Invalid product sample'));
		}
		$options = array('conditions' => array('ProductSample.' . $this->ProductSample->primaryKey => $id));
		$this->set('productSample', $this->ProductSample->find('first', $options));

	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->ProductSample->create();
			if ($this->ProductSample->save($this->request->data)) {
				$this->Flash->success(__('The product sample has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The product sample could not be saved. Please, try again.'));
			}
		}
		// $products = $this->ProductSample->Product->find('list');
		// $this->set(compact('products'));

		// $this->Product->recursive = 0;
		$this->set('products', $this->Paginator->paginate());

		
		$this->loadModel('ProductCategory');
		$this->loadModel('Product');

		
		$productCategory = $this->ProductCategory->find('all');
		$product = $this->Product->find('all');

		
		$this->set("categories", $productCategory);
		$this->set("products", $product);
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->ProductSample->exists($id)) {
			throw new NotFoundException(__('Invalid product sample'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->ProductSample->save($this->request->data)) {
				$this->Flash->success(__('The product sample has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The product sample could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('ProductSample.' . $this->ProductSample->primaryKey => $id));
			$this->request->data = $this->ProductSample->find('first', $options);
		}
		$products = $this->ProductSample->Product->find('list');
		$this->set(compact('products'));
		
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->ProductSample->exists($id)) {
			throw new NotFoundException(__('Invalid product sample'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->ProductSample->delete($id)) {
			$this->Flash->success(__('The product sample has been deleted.'));
		} else {
			$this->Flash->error(__('The product sample could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
