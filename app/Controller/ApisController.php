<?php
App::uses('AppController', 'Controller');
/**
 * ProductCategories Controller
 *
 * @property ProductCategory $ProductCategory
 * @property PaginatorComponent $Paginator
 */
class ApisController extends AppController {

    public function index() {
        $this->autoRender = false;
	}	
	
    public function hit($format = null) {
        $this->layout = false;
	    //set default response
		$response = array("data" => array());
		$text = '<?xml version="1.0" encoding="utf-8"?><data></data>';
		
		$nama = "";
		$status = "";

		// cek format
	    if(!empty($format)){
				// cek params
				if (!empty($this->request->data['product_id'])) {
					
                    $product_id = $this->request->data['product_id'];
                    
                    $this->loadModel('ProductLog');

					$result = $this->ProductLog->query("SELECT p.name, pl.status from product_logs AS pl INNER JOIN products AS p ON p.id = pl.product_id  where p.code = '$product_id' order by pl.id DESC limit 1");
					// cek database
					if(!empty($result)){

						if ($format == "txn.json") {
							$response["data"]= $result;  
						} else if($format == "txn.xml") {

							$nama 	= $result[0][0]['name'];
							$status = $result[0][0]['status'];
							$text = '<?xml version="1.0" encoding="utf-8"?>
									<data>
										<name>'.$nama.'</name>
										<status>'.$status.'</status>
									</data>
									';
						} else {
							echo "ra ono formate";
						}
					}  
				}
	    } else {
			echo "Not Found";
        }

		$this->response->type('application/json');
		$this->response->body(json_encode($response));

		if ($format == "txn.json") {
			return $this->response->send();
		} else if($format == "txn.xml") {
			$this->response->type('text/xml');
			echo $text;
		}	    
    }



    public function jajal() {
		$this->autoRender = false;
        
        $params['product_id'] = '80'; // 80 = PLN Prepaid

		$output = $this->hitApi($params);
		
        echo "<pre>";
        print_r($output);
    }
    
    public function hitApi($params = array()) 
    {
		$this->pel_url = "http://180.211.90.243:5000/Apis/hit/txn.json";

		$product_id = $params['product_id'];

		$post_data = array(
			'product_id' => $product_id,
        );
        
        $post_data = http_build_query($post_data, '', '&');
		$curl = curl_init($this->pel_url);

		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($curl, CURLOPT_VERBOSE, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_URL, $this->pel_url);
		curl_setopt($curl, CURLOPT_HEADER, false);
		// curl_setopt($curl, CURLOPT_HTTPHEADER, array(
		//     'Authorization: PELANGIREST username='.$this->pel_user.'&password='.$this->pel_pass.'&signature='.$signature
		// ));
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $post_data);

		$response = curl_exec($curl);
		$info = curl_getinfo($curl);

		curl_close($curl);

		// $output = json_decode($response, true);
		return $response;
	}
    

}
