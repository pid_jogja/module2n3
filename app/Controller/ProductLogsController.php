<?php
App::uses('AppController', 'Controller');
App::uses('CakeEmail','Network/Email');
/**
 * ProductLogs Controller
 *
 * @property ProductLog $ProductLog
 * @property PaginatorComponent $Paginator
 */
class ProductLogsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->ProductLog->recursive = 2;

		$pl = $this->ProductLog->query("SELECT *, product_categories.name as categories, products.name as product FROM product_logs 
		INNER JOIN products ON products.id = product_logs.product_id
		INNER JOIN product_categories ON product_categories.id = products.product_category_id
		WHERE product_logs.id IN (
			SELECT MAX(product_logs.id) AS id FROM product_logs GROUP BY product_logs.idpel
		)");
		
		$this->set("productLogs", $pl);
	}

	public function report_pay() {
		$this->ProductLog->recursive = 2;

		/*$pl = $this->ProductLog->query("SELECT *, product_categories.name as categories, products.name as product FROM product_logs 
		INNER JOIN products ON products.id = product_logs.product_id
		INNER JOIN product_categories ON product_categories.id = products.product_category_id
		WHERE product_logs.id IN (
			SELECT MAX(product_logs.id) AS id FROM product_logs GROUP BY product_logs.idpel
		)");
		
		$this->set("productLogs", $pl);*/
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->ProductLog->exists($id)) {
			throw new NotFoundException(__('Invalid product log'));
		}
		$options = array('conditions' => array('ProductLog.' . $this->ProductLog->primaryKey => $id));
		$this->set('productLog', $this->ProductLog->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->ProductLog->create();
			if ($this->ProductLog->save($this->request->data)) {
				$this->Flash->success(__('The product log has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The product log could not be saved. Please, try again.'));
			}
		}
		$products = $this->ProductLog->Product->find('list');
		$this->set(compact('products'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->ProductLog->exists($id)) {
			throw new NotFoundException(__('Invalid product log'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->ProductLog->save($this->request->data)) {
				$this->Flash->success(__('The product log has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The product log could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('ProductLog.' . $this->ProductLog->primaryKey => $id));
			$this->request->data = $this->ProductLog->find('first', $options);
		}
		$products = $this->ProductLog->Product->find('list');
		$this->set(compact('products'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->ProductLog->exists($id)) {
			throw new NotFoundException(__('Invalid product log'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->ProductLog->delete($id)) {
			$this->Flash->success(__('The product log has been deleted.'));
		} else {
			$this->Flash->error(__('The product log could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

	public function mail() {
		$this->autoRender = false;

		$idpel  = isset($this->request->query['idpel']) ? $this->request->query['idpel'] : null;
		$product  = isset($this->request->query['product']) ? $this->request->query['product'] : null;
		$category  = isset($this->request->query['category']) ? $this->request->query['category'] : null;
		$status  = isset($this->request->query['status']) ? $this->request->query['status'] : null;
		// $response_date  = isset($this->request->query['$response_date']) ? $this->request->query['$response_date'] : null;
		 

        $Email = new CakeEmail('smtp');
		$Email->to(array('devpelangijogja@gmail.com')) //,'co.pelangi@gmail.com','emai3@gmail.com'
				->subject('Terjadi Gangguan')
				->send(" 
ID Pelanggan : $idpel
Produk :$product
Kategori : $category
Status : $status
					");
	    $this->Flash->success(__('Send Email Success'));
		$this->redirect(array('action'=>'index'));
	}

	public function mailManual($idpel, $product, $category, $status) {
		$this->autoRender = false;
        $Email = new CakeEmail('smtp');
		$Email->to(array('devpelangijogja@gmail.com')) //,'co.pelangi@gmail.com','emai3@gmail.com'
				->subject('Terjadi Gangguan')
				->send(" 
ID Pelanggan : $idpel
Produk :$product
Kategori : $category
Status : $status
					");
	}

	public function generateTrxId($length) {
        return substr(str_shuffle("0123456789"), 0, $length);
    }

    public function hit() {
		$this->autoRender = false;
		if (!empty(($this->request->data['idpel']))) {

			$periode_payment = '';
			$idpel 		= (String) $this->request->data['idpel'];

			// explode category
			$data_category = $this->request->data['categories'];
			$exp = explode("-", $data_category);
			$category = $exp[1];

			$data_product = $this->request->data['product'];
			$exp = explode("-", $data_product);
			$product_id = $exp[0];
			$product = $exp[1];

			if (!empty($this->request->data['periode'])) {
				$periode_payment = (String) $this->request->data['periode'];
			} 

			$params['trx_date'] = date("YmdHis");
			$params['trx_id'] = $this->generateTrxId(10);
			$params['trx_type'] = '2100'; // 2100 = Inquiry, 2200 = Payment
			$params['cust_msisdn'] = '01428800711';
			$params['cust_account_no'] = $idpel;
			$params['product_id'] = (String) $product_id; // 80 = PLN Prepaid
			$params['product_nomination'] = ''; 
			// $params['product_nomination'] = '20000'; // use this line to makes payment request
			$params['periode_payment'] = $periode_payment;
			$params['unsold'] = '';

			$input = json_encode($params, true);

			$request_date = date('Y-m-d H:i:s');

			$output = $this->hitApi($params);
			$response_date = date('Y-m-d H:i:s');

			$parse 		= json_decode($output, true);
			// print_r($parse);

			$data 		= $parse['data']['trx'];
			$getrc = $data['rc'];

			// $success_rc = ['0000', '0014', '0034', '0088', '0089', '0047', '0013', '0041', '0042', '0090', '0063', '0094', '0012'];
			$success_rc = Configure::read('rc');

			$rc = "Gangguan";

			if (in_array($getrc, $success_rc)) {
				$rc = "Normal";
			}

			if ($rc == 'Normal') {
				$response['category'] 	= $category;
				$response['product'] 	= $product;
				$response['idpel'] 		= $this->request->data['idpel'];
				$response['rc']			= $getrc;
				$response['status']		= $rc;
				$response['response']	= true;
				echo json_encode($response);
			} else {
				$response['category'] 	= $category;
				$response['product'] 	= $product;
				$response['idpel'] 		= $this->request->data['idpel'];
				$response['rc']			= $getrc;
				$response['status']		= $rc;
				$response['response']	= false;
				echo json_encode($response);
				// $this->mailManual($this->request->data['idpel'], $product, $category, $rc );
			}
		} else {
			$response['emptyid']	= false;
			echo json_encode($response);
		}
	}
	

	public function hitApi($params = array()) {
		$this->pel_user = Configure::read('api.username');
		$this->pel_pass = Configure::read('api.password');
		$this->pel_key = Configure::read('api.key');
		$this->pel_url = Configure::read('api.server');


        $trx_date = $params['trx_date'];
		$trx_id = $params['trx_id'];
		$trx_type = $params['trx_type'];
		$cust_msisdn = $params['cust_msisdn'];
		$cust_account_no = $params['cust_account_no'];
		$product_id = $params['product_id'];
		$product_nomination = $params['product_nomination'];
		$periode_payment = $params['periode_payment'];
		$unsold = $params['unsold'];

		$signature = md5($this->pel_user . $this->pel_pass . $product_id . $trx_date . $this->pel_key);

		$post_data = array(
			'trx_date' => $trx_date,
			'trx_type' => $trx_type,
			'trx_id' => $trx_id,
			'cust_msisdn' => $cust_msisdn,
			'cust_account_no' => $cust_account_no,
			'product_id' => $product_id,
			'product_nomination' => $product_nomination,
			'periode_payment' => $periode_payment
			// 'unsold' => '1'
        );
        
        $post_data = http_build_query($post_data, '', '&');
		$curl = curl_init($this->pel_url);

		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($curl, CURLOPT_VERBOSE, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_URL, $this->pel_url);
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array(
		    'Authorization: PELANGIREST username='.$this->pel_user.'&password='.$this->pel_pass.'&signature='.$signature
		));
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $post_data);

		$response = curl_exec($curl);
		$info = curl_getinfo($curl);

		curl_close($curl);

		// $output = json_decode($response, true);
		return $response;
	}

	
}
