<?php
App::uses('AppController', 'Controller');
/**
 * Products Controller
 *
 * @property Product $Product
 * @property PaginatorComponent $Paginator
 */
class ProductsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Product->recursive = 0;
		$this->set('products', $this->Paginator->paginate());

		
		$this->loadModel('ProductCategory');

		
		$productCategory = $this->ProductCategory->find('all');
		$product = $this->Product->find('all');

		
		$this->set("categories", $productCategory);
		$this->set("products", $product);
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function look() {
		$this->Product->recursive = 0;
		$p=$this->Product->find('all');
		$this->set('products', $p);
	}

	public function view($id = null) {
		if (!$this->Product->exists($id)) {
			throw new NotFoundException(__('Invalid product'));
		}
		$options = array('conditions' => array('Product.' . $this->Product->primaryKey => $id));
		$this->set('product', $this->Product->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Product->create();
			if ($this->Product->save($this->request->data)) {
				$this->Flash->success(__('The product has been saved.'));
				return $this->redirect(array('action' => 'look'));
			} else {
				$this->Flash->error(__('The product could not be saved. Please, try again.'));
			}
		}
		// $productCategories = $this->Product->ProductCategory->find('list');
		// $this->set(compact('productCategories'));

		$this->set('products', $this->Paginator->paginate());

		
		$this->loadModel('ProductCategory');
		$this->loadModel('Product');

		
		$productCategory = $this->ProductCategory->find('all');
		$product = $this->Product->find('all');

		
		$this->set("categories", $productCategory);
		$this->set("products", $product);
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Product->exists($id)) {
			throw new NotFoundException(__('Invalid product'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Product->save($this->request->data)) {
				$this->Flash->success(__('The product has been saved.'));
				return $this->redirect(array('action' => 'look'));
			} else {
				$this->Flash->error(__('The product could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Product.' . $this->Product->primaryKey => $id));
			$this->request->data = $this->Product->find('first', $options);
		}
		$productCategories = $this->Product->ProductCategory->find('list');
		$this->set(compact('productCategories'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->Product->exists($id)) {
			throw new NotFoundException(__('Invalid product'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Product->delete($id)) {
			$this->Flash->success(__('The product has been deleted.'));
		} else {
			$this->Flash->error(__('The product could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'look'));
	}

	public function getProducts()
    {
		$this->autoRender = false;
		 
		$data = $this->request->data('id');

		$expl = explode("-", $data);
		$id = $expl[0];
		$name = $expl[1];

		$products = $this->Product->find('all', array(
			'recursive' => -1,
			'fields' => array('Product.name', 'Product.code'),
			'conditions' => array('Product.product_category_id' => $id)
			)
		);

		echo json_encode($products);
    }

    

}
