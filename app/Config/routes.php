<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.2.9
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */
 
/**
 * Here, we are connecting '/' (base path) to controller called 'Pages',
 * its action called 'display', and we pass a param to select the view file
 * to use (in this case, /app/View/Pages/home.ctp)...
 */
	Router::connect('/', array('controller' => 'ProductLogs'));
/**
 * ...and connect the rest of 'Pages' controller's URLs.
 */
	// Router::connect('/pages/*', array('controller' => 'productlogs'));
	Router::connect('/inquiry', array('controller' => 'ProductLogs'));
	Router::connect('/payment', array('controller' => 'ProductLogs', 'action' => 'report_pay'));
	Router::connect('/hit', array('controller' => 'ProductLogs', 'action' => 'hit'));
	Router::connect('/hit', array('controller' => 'Apis', 'action' => 'hit'));

	
	Router::connect('/check-status', array('controller' => 'Products', 'action' => 'index'));

	Router::connect('/input', array('controller' => 'ProductSamples', 'action' => 'add'));
	Router::connect('/inputp', array('controller' => 'Products', 'action' => 'add'));
	Router::connect('/inputc', array('controller' => 'ProductCategories', 'action' => 'add'));
	Router::connect('/lookp', array('controller' => 'Products', 'action' => 'look'));

	Router::connect('/pelanggan', array('controller' => 'ProductSamples', 'action' => 'index'));
	Router::connect('/listp', array('controller' => 'Products', 'action' => 'list'));
	Router::connect('/listpc', array('controller' => 'ProductCategories', 'action' => 'index'));

	// Router::connect('/ProductSamples/edit/', array('controller' => 'ProductSamples', 'action' => 'edit'));
	Router::connect('/editp/', array('controller' => 'Products', 'action' => 'edit'));
	Router::connect('/editpc/', array('controller' => 'ProductCategories', 'action' => 'edit'));

	
	Router::connect('/Transaction/:val', array('controller' => 'ProductLogs', 'action' => 'rest'));

	// Router::parseExtensions('json', 'xml');


	// transaksi api
	// Router::connect(
	// 	'/Trans/:val', 
	// 	array(
	// 		'controller' => 'ProductLogs',
	// 		'action' => 'rest',
	// 	), 
	// 	array(
	// 		'pass' => array('val')
	// 	)
	// );

	// Router::connect(
	// 	'/Transaction/data',
	// 	array('controller' => 'Transaction', 'action' => 'rest', 'ext' => 'json')
	// );

/**
 * Load all plugin routes. See the CakePlugin documentation on
 * how to customize the loading of plugin routes.
 */
	CakePlugin::routes();

/**
 * Load the CakePHP default routes. Only remove this if you do not want to use
 * the built-in default routes.
 */
	require CAKE . 'Config' . DS . 'routes.php';
